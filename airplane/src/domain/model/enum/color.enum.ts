export enum Color {
  WHITE = 'WHITE',
  ORANGE = 'ORANGE',
  GREEN = 'GREEN',
  BLUE = 'BLUE',
  BROWN = 'BROWN',
  BLACK = 'BLACK',
}
