import { Inject, Injectable } from '@nestjs/common';

import { AirplaneNotFoundError } from 'application/errors/airplane-not-found.error';
import { Airplane } from 'domain/model/airplane.model';
import { AirplaneRepositoryPort } from 'domain/port/airplane-repository.port';

@Injectable()
export class GetAirplaneByIdUseCase {
  constructor(
    @Inject('AirplaneRepositoryPort')
    private readonly airplaneRepositoryPort: AirplaneRepositoryPort,
  ) {}

  async handler(id: string): Promise<Airplane> {
    const response: Airplane = await this.airplaneRepositoryPort.findAirplaneById(id);

    if (!response) throw new AirplaneNotFoundError(id);
    return response;
  }
}
