import { Inject, Injectable } from '@nestjs/common';

import { AirplaneRepositoryPort } from 'domain/port/airplane-repository.port';
import { Airplane } from 'domain/model/airplane.model';

@Injectable()
export class GetAllAirplanesUseCase {
  constructor(
    @Inject('AirplaneRepositoryPort')
    private readonly airplaneRepositoryPort: AirplaneRepositoryPort,
  ) {}

  async handler(name?: string): Promise<Airplane[]> {
    const array: Airplane[] = await this.airplaneRepositoryPort.findAllAirplanes(name);
    return array ?? [];
  }
}
