import { Inject, Injectable } from '@nestjs/common';
import { DateTime } from 'luxon';
import { randomUUID } from 'crypto';

import { CreateAirplaneDto } from 'application/dto/create-airplane.dto';
import { Airplane } from 'domain/model/airplane.model';
import { AirplaneRepositoryPort } from 'domain/port/airplane-repository.port';
import { DatabaseError } from 'infrastructure/utils/errors/database.error';

@Injectable()
export class SaveAirplaneUseCase {
  constructor(
    @Inject('AirplaneRepositoryPort')
    private readonly airplaneRepositoryPort: AirplaneRepositoryPort,
  ) {}

  async handler(dto: CreateAirplaneDto): Promise<void> {
    const myAirplane: Airplane = {
      id: randomUUID(),
      name: dto.name,
      horsePower: dto.horsePower,
      color: dto.color,
      capacity: dto.capacity,
      creationDate: DateTime.now(),
    };

    try {
      await this.airplaneRepositoryPort.saveAirplane(myAirplane);
    } catch (e) {
      throw new DatabaseError();
    }
  }
}
