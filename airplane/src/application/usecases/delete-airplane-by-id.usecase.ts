import { Inject, Injectable } from '@nestjs/common';

import { AirplaneNotFoundError } from 'application/errors/airplane-not-found.error';
import { AirplaneRepositoryPort } from 'domain/port/airplane-repository.port';

@Injectable()
export class DeleteAirplaneByIdUseCase {
  constructor(
    @Inject('AirplaneRepositoryPort')
    private readonly airplaneRepositoryPort: AirplaneRepositoryPort,
  ) {}

  async handler(id: string): Promise<void> {
    const isDeleted: boolean = await this.airplaneRepositoryPort.deleteAirplaneById(id);
    if (!isDeleted) throw new AirplaneNotFoundError(id);
  }
}
