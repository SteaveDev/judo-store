export class AirplaneNotFoundError extends Error {
  constructor(id: string) {
    super(`Airplane was not found. id=${id}`);
  }
}
