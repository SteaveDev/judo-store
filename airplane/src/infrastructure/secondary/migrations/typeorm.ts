import { DataSource } from 'typeorm';

import { InitialTableCreation1659457760886 } from 'infrastructure/secondary/migrations/1659457760886-initial-table-creation';

export const dataSource = async () =>
  new DataSource({
    type: 'postgres',
    host: process.env.DATABASE_HOST ?? 'localhost',
    port: +(process.env.DATABASE_PORT ?? '5442'),
    username: process.env.DATABASE_USER ?? 'airplane',
    database: process.env.DATABASE_NAME || 'airplane',
    password: process.env.ENVIRONMENT ?? 'local',
    migrationsRun: true,
    migrations: [InitialTableCreation1659457760886],
  }).initialize();
