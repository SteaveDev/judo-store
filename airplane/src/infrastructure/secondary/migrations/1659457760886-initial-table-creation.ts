import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class InitialTableCreation1659457760886 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'airplane',
        columns: [
          { name: 'id', type: 'uuid', isNullable: false, isPrimary: true, isUnique: true },
          { name: 'name', type: 'text', isNullable: false },
          { name: 'horse_power', type: 'integer', isNullable: false },
          { name: 'color', type: 'text', isNullable: false },
          { name: 'capacity', type: 'integer', isNullable: false },
          { name: 'creation_date', type: 'timestamp with time zone', isNullable: false },
        ],
      }),
      true,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('airplane');
  }
}
