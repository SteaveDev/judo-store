import { Repository } from 'typeorm';

import { AirplaneNotFoundError } from 'application/errors/airplane-not-found.error'; // TODO dans application ?

import { AirplaneRepositoryPort } from 'domain/port/airplane-repository.port';
import { Airplane } from 'domain/model/airplane.model';

import { AirplaneEntity } from 'infrastructure/secondary/entity/airplane.entity';
import { airplaneEntityToDomain } from 'infrastructure/secondary/mapper/airplane-entity-to-domain.mapper';

export class AirplaneRepository extends Repository<AirplaneEntity> implements AirplaneRepositoryPort {
  async findAllAirplanes(): Promise<Airplane[]> {
    const airplanesEntities: AirplaneEntity[] = await this.find({
      where: {},
    });

    return airplanesEntities.map(airplaneEntityToDomain);
  }

  async findAirplaneById(id: string): Promise<Airplane> {
    const airplaneEntity: AirplaneEntity | null = await this.findOne({
      where: { id },
    });

    if (!airplaneEntity) throw new AirplaneNotFoundError(id);

    return airplaneEntityToDomain(airplaneEntity);
  }

  async saveAirplane(entity: Airplane): Promise<void> {
    const airplaneEntity: AirplaneEntity = this.create(airplaneEntityToDomain(entity));
    await this.save(airplaneEntity);
  }

  async deleteAirplaneById(id: string): Promise<boolean> {
    const airplaneEntity = await this.findOne({ where: { id } });
    if (!airplaneEntity) return false;

    await this.remove(airplaneEntity);
    return true;
  }
}
