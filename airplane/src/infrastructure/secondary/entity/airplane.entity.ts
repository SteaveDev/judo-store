import { Column, Entity, PrimaryColumn } from 'typeorm';
import { DateTime } from 'luxon';

import { DateTimeTransformer } from 'infrastructure/utils/datetime.transformer';

@Entity('airplane')
export class AirplaneEntity {
  @PrimaryColumn({ name: 'id', type: 'uuid' })
  id!: string;

  @Column({ name: 'name' })
  name!: string;

  @Column({ name: 'horse_power' })
  horsePower!: number;

  @Column({ name: 'color' })
  color!: string;

  @Column({ name: 'capacity' })
  capacity!: number;

  @Column({ name: 'creation_date', type: 'timestamptz', transformer: new DateTimeTransformer() })
  creationDate!: DateTime;
}
