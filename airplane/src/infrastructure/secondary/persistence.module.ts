import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';

import { AirplaneEntity } from 'infrastructure/secondary/entity/airplane.entity';
import { AirplaneRepository } from 'infrastructure/secondary/repository/airplane.repository';

@Module({
  imports: [TypeOrmModule.forFeature([AirplaneEntity])],
  providers: [
    {
      provide: 'AirplaneRepositoryPort',
      useFactory: (dataSource: DataSource) => {
        const airplaneRepository = dataSource.getRepository(AirplaneEntity);
        return new AirplaneRepository(
          airplaneRepository.target,
          airplaneRepository.manager,
          airplaneRepository.queryRunner,
        );
      },
      inject: [DataSource],
    },
  ],
  exports: ['AirplaneRepositoryPort'],
})
export default class PersistenceModule {}
