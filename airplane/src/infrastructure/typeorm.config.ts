import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModuleAsyncOptions, TypeOrmModuleOptions } from '@nestjs/typeorm';

import { InitialTableCreation1659457760886 } from 'infrastructure/secondary/migrations/1659457760886-initial-table-creation';

export default class TypeOrmConfig {
  static async getOrmConfig(configService: ConfigService): Promise<TypeOrmModuleOptions> {
    return {
      type: 'postgres',
      host: configService.get('DATABASE_HOST'),
      port: configService.get('DATABASE_PORT'),
      username: configService.get('DATABASE_USER'),
      password: configService.get('DATABASE_PASS'),
      database: configService.get('DATABASE_NAME'),
      autoLoadEntities: true,
      migrationsRun: true,
      migrations: [InitialTableCreation1659457760886],
    };
  }
}

export const typeOrmConfigAsync: TypeOrmModuleAsyncOptions = {
  imports: [ConfigModule],
  useFactory: async (configService: ConfigService): Promise<TypeOrmModuleOptions> =>
    TypeOrmConfig.getOrmConfig(configService),
  inject: [ConfigService],
};
