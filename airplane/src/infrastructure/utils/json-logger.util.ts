type LoggingLevel = 'INFO' | 'ERROR' | 'WARN' | 'DEBUG';

interface FullLogMessage {
  origin: string; // Who logs
  datetime: Date; // When
  level: LoggingLevel; // How
  context: string; // About what are we logging, Uppercase & no whitespace
  message: string;
  details?: object | string;
}

export class JsonLoggerUtil {
  private readonly debugMode: boolean;
  private readonly name: string;

  constructor(name: string) {
    this.debugMode = (process.env.DEBUG ?? 'false') === 'true';
    this.name = name;
  }

  generateLogMessage(level: LoggingLevel, context: string, message: string, details?: object | string): string {
    const logMessage: FullLogMessage = {
      origin: this.name,
      datetime: new Date(),
      level,
      context: context.toUpperCase(),
      message,
      details,
    };
    return JSON.stringify(logMessage);
  }

  info(context: string, message: string, details?: object | string): void {
    console.log(this.generateLogMessage('INFO', context, message, details));
  }

  warn(context: string, message: string, details?: object | string): void {
    console.warn(this.generateLogMessage('WARN', context, message, details));
  }

  error(context: string, message: string, details?: object | string): void {
    console.error(this.generateLogMessage('ERROR', context, message, details));
  }

  debug(context: string, message: string, details?: object | string): void {
    if (this.debugMode) {
      console.log(this.generateLogMessage('DEBUG', context, message, details));
    }
  }
}
