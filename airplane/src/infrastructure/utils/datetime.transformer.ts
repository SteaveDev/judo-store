import { DateTime } from 'luxon';
import { ValueTransformer } from 'typeorm';

export class DateTimeTransformer implements ValueTransformer {
  public from(value: string): DateTime {
    return DateTime.fromJSDate(new Date(value));
  }

  public to(value: string): string {
    return value;
  }
}
