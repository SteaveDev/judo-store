import { Module } from '@nestjs/common';

import PersistenceModule from 'infrastructure/secondary/persistence.module';
import { AirplaneController } from 'infrastructure/primary/controllers/airplane.controller';

import { GetAllAirplanesUseCase } from 'application/usecases/get-all-airplanes.usecase';
import { GetAirplaneByIdUseCase } from 'application/usecases/get-airplane-by-id.usecase';
import { SaveAirplaneUseCase } from 'application/usecases/save-airplane.usecase';
import { DeleteAirplaneByIdUseCase } from 'application/usecases/delete-airplane-by-id.usecase';

@Module({
  imports: [PersistenceModule],
  controllers: [AirplaneController],
  providers: [GetAllAirplanesUseCase, GetAirplaneByIdUseCase, SaveAirplaneUseCase, DeleteAirplaneByIdUseCase],
})
export default class InfrastructureModule {}
