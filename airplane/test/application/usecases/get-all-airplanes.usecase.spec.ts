import { Test, TestingModule } from '@nestjs/testing';
import { DateTime } from 'luxon';

import { GetAllAirplanesUseCase } from 'application/usecases/get-all-airplanes.usecase';
import { Color } from 'domain/model/enum/color.enum';
import { AirplaneRepositoryPort } from 'domain/port/airplane-repository.port';
import { GetAirplaneRequest } from 'infrastructure/primary/request/get-airplane.request';

describe('GetAllAirplanesUseCase', () => {
  let getAllAirplanesUseCase: GetAllAirplanesUseCase;
  let airplaneRepositoryPort: AirplaneRepositoryPort;

  const airplaneRepositoryPortMock = jest.fn(() => ({
    findAllAirplanes: jest.fn(() => []),
  }));

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GetAllAirplanesUseCase,
        {
          provide: 'AirplaneRepositoryPort',
          useFactory: airplaneRepositoryPortMock,
        },
      ],
    }).compile();

    getAllAirplanesUseCase = module.get<GetAllAirplanesUseCase>(GetAllAirplanesUseCase);
    airplaneRepositoryPort = module.get<AirplaneRepositoryPort>('AirplaneRepositoryPort');
  });

  describe('handler', () => {
    it('should return empty array', async () => {
      // GIVEN
      const airplanes: GetAirplaneRequest[] = [];

      // WHEN
      jest.spyOn(airplaneRepositoryPort, 'findAllAirplanes').mockResolvedValue(airplanes);
      const result = await getAllAirplanesUseCase.handler();

      // THEN
      expect(airplaneRepositoryPort.findAllAirplanes).toHaveBeenCalledTimes(1);
      expect(result).toStrictEqual(airplanes);
      expect(result.length).toEqual(0);
    });

    it('should return all airplanes', async () => {
      // GIVEN
      const airplanes: GetAirplaneRequest[] = [
        {
          id: '098e59ff-03cb-4b25-8d0d-9acb576ba9fb',
          name: 'Airbus 666',
          horsePower: 2200,
          color: Color.BLUE,
          capacity: 380,
          creationDate: DateTime.fromISO('2021-01-01T00:01:00.000Z').toUTC(),
        },
        {
          id: 'e2093388-05d1-4ead-afbe-90a13997a515',
          name: 'Airbus 777',
          horsePower: 2500,
          color: Color.WHITE,
          capacity: 450,
          creationDate: DateTime.fromISO('2022-01-01T00:01:00.000Z').toUTC(),
        },
      ];

      // WHEN
      jest.spyOn(airplaneRepositoryPort, 'findAllAirplanes').mockResolvedValue(airplanes);
      const result = await getAllAirplanesUseCase.handler();

      // THEN
      expect(result).toStrictEqual(airplanes);
      expect(result.length).toEqual(2);
    });

    it('should only return Airbus 777', async () => {
      // GIVEN
      const expectedResponse: GetAirplaneRequest[] = [
        {
          id: 'e2093388-05d1-4ead-afbe-90a13997a515',
          name: 'Airbus 777',
          horsePower: 2500,
          color: Color.WHITE,
          capacity: 450,
          creationDate: DateTime.fromISO('2022-01-01T00:01:00.000Z').toUTC(),
        },
      ];

      // WHEN
      const myMock = jest.spyOn(airplaneRepositoryPort, 'findAllAirplanes').mockResolvedValue(expectedResponse);
      const result = await getAllAirplanesUseCase.handler('Airbus 777');

      // THEN
      expect(myMock).toHaveBeenCalledWith('Airbus 777');
      expect(result).toStrictEqual(expectedResponse);
      expect(result.length).toEqual(1);
    });
  });
});
