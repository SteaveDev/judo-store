import { Test, TestingModule } from '@nestjs/testing';

import { SaveAirplaneUseCase } from 'application/usecases/save-airplane.usecase';
import { CreateAirplaneDto } from 'application/dto/create-airplane.dto';
import { AirplaneRepositoryPort } from 'domain/port/airplane-repository.port';
import { Color } from 'domain/model/enum/color.enum';

describe('SaveAirplaneUseCase', () => {
  let saveAirplaneUseCase: SaveAirplaneUseCase;
  let airplaneRepositoryPort: AirplaneRepositoryPort;

  const airplaneRepositoryPortMock = jest.fn(() => ({
    saveAirplane: jest.fn(() => Promise.resolve()),
  }));

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SaveAirplaneUseCase,
        {
          provide: 'AirplaneRepositoryPort',
          useFactory: airplaneRepositoryPortMock,
        },
      ],
    }).compile();

    airplaneRepositoryPort = module.get<AirplaneRepositoryPort>('AirplaneRepositoryPort');
    saveAirplaneUseCase = module.get<SaveAirplaneUseCase>(SaveAirplaneUseCase);
  });

  describe('handler', () => {
    it('should create new nebefCodeMethod', async () => {
      // GIVEN
      const airplaneDto: CreateAirplaneDto = {
        name: 'Airbus 777',
        horsePower: 2500,
        color: Color.BLUE,
        capacity: 450,
      };

      // WHEN
      await saveAirplaneUseCase.handler(airplaneDto);

      // THEN
      expect(airplaneRepositoryPort.saveAirplane).toHaveBeenCalledTimes(1);
      expect(airplaneRepositoryPort.saveAirplane).toHaveBeenCalledWith(
        expect.objectContaining({
          name: 'Airbus 777',
          horsePower: 2500,
          color: Color.BLUE,
          capacity: 450,
        }),
      );
    });
  });
});
