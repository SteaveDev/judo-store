import { Test, TestingModule } from '@nestjs/testing';
import { DateTime } from 'luxon';

import { AirplaneNotFoundError } from 'application/errors/airplane-not-found.error';
import { GetAirplaneByIdUseCase } from 'application/usecases/get-airplane-by-id.usecase';
import { AirplaneRepositoryPort } from 'domain/port/airplane-repository.port';
import { Airplane } from 'domain/model/airplane.model';
import { Color } from 'domain/model/enum/color.enum';

describe('GetAirplaneByIdUseCase', () => {
  let getAirplaneByIdUseCase: GetAirplaneByIdUseCase;
  let airplaneRepositoryPort: AirplaneRepositoryPort;

  const airplaneRepositoryPortMock = jest.fn(() => ({
    findAirplaneById: jest.fn(() => {}),
  }));

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GetAirplaneByIdUseCase,
        {
          provide: 'AirplaneRepositoryPort',
          useFactory: airplaneRepositoryPortMock,
        },
      ],
    }).compile();

    getAirplaneByIdUseCase = module.get<GetAirplaneByIdUseCase>(GetAirplaneByIdUseCase);
    airplaneRepositoryPort = module.get<AirplaneRepositoryPort>('AirplaneRepositoryPort');
  });

  describe('handler', () => {
    it('should return existing nebef code methods', async () => {
      // GIVEN
      const uuid: string = '94ee2532-4b0b-4c1b-b6bf-2cc59812ce87';
      const nebefCodeMethods: Airplane = {
        id: uuid,
        name: 'Airbus 777',
        horsePower: 2500,
        color: Color.BLUE,
        capacity: 450,
        creationDate: DateTime.fromISO('2022-01-01T00:01:00.000Z').toUTC(),
      };

      // WHEN
      const myMock = jest.spyOn(airplaneRepositoryPort, 'findAirplaneById').mockResolvedValue(nebefCodeMethods);
      const result = await getAirplaneByIdUseCase.handler(uuid);

      // THEN
      expect(myMock).toHaveBeenCalledWith(uuid);
      expect(result).toStrictEqual(nebefCodeMethods);
    });

    it('should throw error when airplane is not found', async () => {
      // GIVEN
      const uuid = '285deaf2-2871-11ed-a261-0242ac120002';

      // WHEN THEN
      await expect(getAirplaneByIdUseCase.handler(uuid)).rejects.toThrow(new AirplaneNotFoundError(uuid));
    });
  });
});
